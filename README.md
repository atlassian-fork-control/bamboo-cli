# Bamboo Command Line interface

Use Atlassian Bamboo from the command line

## Install

* `sudo gem install bamboo-cli`
* set `BAMBOO_URL`, `BAMBOO_USERNAME` and `BAMBOO_PASSWORD` environment variables


## Commands 

    bamboo-cli help [TASK]  # Describe available tasks or one specific task
    bamboo-cli clone KEY NEWKEY  # Clones a plan configuration
    bamboo-cli info KEY     # Gets the info (including if its building or not) of a plan or job
    bamboo-cli me           # Gets information about the current user
    bamboo-cli pause        # Pause the server from building
    bamboo-cli queue KEY    # Queues the given build
    bamboo-cli resume       # Resume the server from pause
    bamboo-cli server       # Gets information about the server

## Examples

### Start a build

`bamboo-cli queue BAM-BOO`

### Start a build and run all Manual Stages

`bamboo-cli queue BAM-BOO --allstages`

### Continue an existing build from a Manual Stage

`bamboo-cli queue BAM-BOO-123 --stage="Deployment"`

### Continue an existing build and run all uncompleted Manual Stages

`bamboo-cli queue BAM-BOO-123 --allstages`

### Clone a build

`bamboo-cli clone BAM-BOO NEW-KEY`

## License

Apache License Version 2.0 http://www.apache.org/licenses/LICENSE-2.0
